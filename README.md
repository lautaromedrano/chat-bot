#Chat Bot

## Description
A basic chat bot that understands some commands. 

Interviewer: Logixboard

### Commands

* !timeat <tzinfo>
* !timepopularity <tzinfo_or_prefix>

## Installation

```bash
$ docker-compose build
```

## Running the app

```bash
$ docker-compose up -d
```
