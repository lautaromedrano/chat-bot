FROM node:latest

RUN mkdir -p /chat-bot
WORKDIR /chat-bot

COPY package*.json ./

RUN npm ci

COPY . .

EXPOSE 3000

CMD ["npm", "run", "start:dev"]