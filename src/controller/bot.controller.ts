import { Controller, Get, Param, Query } from '@nestjs/common';
import { BotService } from '../service/bot.service';

@Controller('message')
export class BotController {
  constructor(private readonly botService: BotService) {}

  @Get()
  parse(@Query('msg') msg: string): Promise<string> {
    return this.botService.parse(msg);
  }
}
