import { MongoService } from "src/db/mongo.service";

export abstract class Command {
    constructor(protected readonly msgParts: string[], protected readonly mongoService: MongoService) {}
    abstract handle(): Promise<string>;
}
