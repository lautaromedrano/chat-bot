import { MongoService } from 'src/db/mongo.service';
import { Command } from './Command';

export class TimePopularity extends Command {

    handle(): Promise<string> {
        let timezone = this.msgParts[1].trim().split(' ')[1];

        return this.mongoService.countRequests(timezone);
    }
}