import { Command } from './Command';

export class NullCommand extends Command {
    handle(): Promise<string> {
        return Promise.resolve('');
    }
}