import { MongoService } from 'src/db/mongo.service';
import { Command } from './Command';

const axios = require('axios');

export class TimeAt extends Command {
    private readonly timeApi: string;

    constructor(msgParts: string[], timeApi: string, mongoService: MongoService) {
        super(msgParts, mongoService);
        this.timeApi = timeApi;
    }

    async handle(): Promise<string> {
        let timezone = this.msgParts[1].trim().split(' ')[1];

        return await axios(this.timeApi + timezone).then(
            (response) => {
                if (response.status != 200) {
                    console.log('Unable to fetch time.');
                    return 'Unknown timezone';
                } else {
                    this.mongoService.saveRequest({ timezone: timezone, timestamp: new Date()});
                    let date = new Date(response.data['datetime']);
                    return date.toLocaleString ();
                }
            }
        ).catch((error: string) => {
            return 'Unknown timezone';
        });
    }
}
