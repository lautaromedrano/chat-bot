import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';

import { BotController } from './controller/bot.controller';
import { MongoService } from './db/mongo.service';
import { BotService } from './service/bot.service';
import { CommandFactoryService } from './service/command.factory.service';

@Module({
  imports: [ConfigModule.forRoot()],
  controllers: [BotController],
  providers: [BotService, CommandFactoryService, MongoService],
})
export class BotModule {}
