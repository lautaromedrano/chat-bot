import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

const mongoose = require('mongoose');

const options = {
    useNewUrlParser: true,
    useFindAndModify: false,
    useCreateIndex: true,
    useUnifiedTopology: true,
    autoIndex: false,
    poolSize: 10,
    bufferMaxEntries: 0
};

export const TimezoneRequest = mongoose.model('TimezoneRequest', { timezone: String, timestamp: Date});

@Injectable()
export class MongoService {

    constructor(private readonly configService: ConfigService) {
        let MONGO_HOSTNAME = this.configService.get<string>('MONGO_HOSTNAME');
        let MONGO_DB = this.configService.get<string>('MONGO_DB');
        let MONGO_PORT = this.configService.get<string>('MONGO_PORT');

        const dbConnectionUrl = {
            'LOCALURL': `mongodb://${MONGO_HOSTNAME}:${MONGO_PORT}/${MONGO_DB}`
        };
        mongoose.connect(dbConnectionUrl.LOCALURL, {useNewUrlParser: true, useUnifiedTopology: true});
        mongoose.connection.on('error', console.error.bind(console, 'Mongodb Connection Error:' + dbConnectionUrl.LOCALURL));
        mongoose.connection.once('open', () => {
            console.log('Mongodb connection succesful');
        });
    }

    saveRequest(data: object): void {
        let timezoneRequest = new TimezoneRequest(data);
        timezoneRequest.save().then(() => console.log('Request saved'));
    }

    countRequests(timezone: string): Promise<string> {
    return TimezoneRequest.countDocuments({ timezone: {'$regex' : `^${timezone}.*`} }, (err, count) => {
            if (err) {
                console.log('Couldn\'t count requests');
            } else {
                return count;
            }
        });
    }
}
