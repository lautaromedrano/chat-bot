import { Injectable } from '@nestjs/common';
import { CommandFactoryService } from './command.factory.service';

@Injectable()
export class BotService {
  constructor(private readonly commandFactoryService: CommandFactoryService) {}

  parse(msg: string): Promise<string> {
    let command = this.commandFactoryService.make(msg);

    return command.handle();
  }
}
