import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { MongoService } from 'src/db/mongo.service';
import { NullCommand } from 'src/model/command/NullCommand';
import { TimeAt } from 'src/model/command/TimeAt';
import { TimePopularity } from 'src/model/command/TimePopularity';
import { Command } from '../model/command/Command';

@Injectable()
export class CommandFactoryService {
  constructor(private readonly configService: ConfigService, private readonly mongoService: MongoService) {}

  make(msg: string): Command {
    let msgParts = this.split(msg);

    if (msgParts[1].indexOf('!timeat') > -1) {
        return new TimeAt(msgParts, this.configService.get<string>('TIME_API'), this.mongoService);
    }
    if (msgParts[1].indexOf('!timepopularity') > -1) {
        return new TimePopularity(msgParts, this.mongoService);
    }

    return new NullCommand(msgParts, this.mongoService);
  }

  split(msg: string): string[] {
    let parts = [];
    let separatorIndex = msg.indexOf(': ');

    // We always get ': ' as separator
    parts.push(msg.substr(0, separatorIndex), msg.substr(separatorIndex + 2));

    return parts;
  }
}
